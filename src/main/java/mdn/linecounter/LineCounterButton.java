 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.linecounter;

import com.pi4j.component.button.ButtonBase;
import com.pi4j.component.button.ButtonState;
import com.pi4j.component.button.ButtonStateChangeEvent;
import com.pi4j.component.button.ButtonStateChangeListener;
import com.pi4j.component.button.impl.GpioButtonComponent;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import mdn.iotlib.comm.message.Message;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class LineCounterButton extends ButtonBase implements ButtonStateChangeListener {

    private static final GpioController GPIO = GpioFactory.getInstance();

    private LineCounterDevice hostDevice;

    private GpioButtonComponent gpioButton;

    private int inputNumber;

    private Date lastPushEvent;
    
    public static LineCounterButton prepareWithHost(LineCounterDevice hostDevice) {
        LineCounterButton btn = new LineCounterButton();
        btn.hostDevice = hostDevice;
        return btn;
    }
    
    public LineCounterButton withNumber(int num) {
        inputNumber = num;
        setName(String.valueOf(num));
        return this;
    }
    
    public LineCounterButton withInput(Input input) {

        GpioPinDigitalInput pin = GPIO.provisionDigitalInputPin(input.getPin(), PinPullResistance.OFF);
        pin.setDebounce(250, PinState.HIGH, PinState.LOW);
        PinState releasedState = input.isInverted() ? PinState.LOW : PinState.HIGH;
        PinState pressedState = input.isInverted() ? PinState.HIGH : PinState.LOW;
        gpioButton = new GpioButtonComponent(pin, releasedState, pressedState);
        gpioButton.addListener(this);

        return this;

    }
    
    public LineCounterButton build() {
//        loadCache();
        return this;
    }

    @Override
    public ButtonState getState() {
        return gpioButton.getState();
    }

    public int getInputNumber() {
        return inputNumber;
    }

    @Override
    public void onStateChange(ButtonStateChangeEvent event) {
        if (event.isPressed()) {
            Date pushEvent = Calendar.getInstance().getTime();
            lastPushEvent = pushEvent;
        } else {
            Date releaseEvent = Calendar.getInstance().getTime();
            long pushDuration = releaseEvent.getTime() - lastPushEvent.getTime();
            Message msg = new Message(8);
            msg.setSource(UUID.fromString(hostDevice.getName()));
            msg.setInput(inputNumber);
            msg.setTimestamp(releaseEvent.getTime());
            msg.setFlag(1);
            msg.putLong(pushDuration);
            hostDevice.queued(msg);
        }
    }


    public void test(int c) {
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                for (int j = 0; j < c; j++) {
                    Message msg = new Message(8);
                    msg.setSource(UUID.fromString(hostDevice.getName()));
                    msg.setInput(inputNumber);
                    msg.setTimestamp(Calendar.getInstance().getTime().getTime());
                    msg.setFlag(1);
                    msg.putLong(100);
                    hostDevice.queued(msg);
//                    Thread.yield();
                    try {
                        TimeUnit.MILLISECONDS.sleep(500);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LineCounterButton.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

    }

}
