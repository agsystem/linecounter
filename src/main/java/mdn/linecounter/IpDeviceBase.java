/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.linecounter;

import com.pi4j.device.DeviceBase;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arief Prihasanto
 */
public abstract class IpDeviceBase extends DeviceBase {

    protected String getIpAddr() {
        String strAddr = "";
        try {
            Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
            while (ifaces.hasMoreElements()) {
                NetworkInterface iface = ifaces.nextElement();
                if (iface.isUp() && !iface.isLoopback() && !iface.isVirtual() && !iface.isPointToPoint()) {
                    Enumeration<InetAddress> addrs = iface.getInetAddresses();
                    while (addrs.hasMoreElements()) {
                        InetAddress addr = addrs.nextElement();
                        if (addr.isSiteLocalAddress()) {
                            strAddr = addr.getHostAddress();
                        }
                    }
                }
            }

            return strAddr;
        } catch (SocketException ex) {
            Logger.getLogger(IpDeviceBase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return strAddr;
        }
    }

    protected String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            return "";
        }
    }
}
