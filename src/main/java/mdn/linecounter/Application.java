/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.linecounter;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arief Prihasanto
 */
public abstract class Application {

    private static Application instance;
    
    private Parameter parameter;

    private volatile boolean stopped = false;

    public static Application getInstance() {
        return instance;
    } 

    public static void launch(String... args) {
        StackTraceElement[] cause = Thread.currentThread().getStackTrace();
        boolean foundThisMethod = false;

        String callingClassName = null;
        for (StackTraceElement se : cause) {
            // Skip entries until we get to the entry for this class
            String className = se.getClassName();
            String methodName = se.getMethodName();
            if (foundThisMethod) {
                callingClassName = className;
                break;
            } else if (Application.class.getName().equals(className)
                    && "launch".equals(methodName)) {

                foundThisMethod = true;
            }
        }

        if (callingClassName == null) {
            throw new RuntimeException("Error: unable to determine Application class");
        }

        try {
            Class theClass = Class.forName(callingClassName, false,
                    Thread.currentThread().getContextClassLoader());
            if (Application.class.isAssignableFrom(theClass)) {
                Class<? extends Application> appClass = theClass;
                Constructor<? extends Application> c = appClass.getConstructor();
                instance = c.newInstance();
                instance.setParameter(new Parameter(args));
                instance.init();
                instance.start();
                instance.loop();
            } else {
                throw new RuntimeException("Error: " + theClass
                        + " is not a subclass of mdn.linecounter.Application");
            }
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    } 

    public abstract String getName();
    
    public void init() throws Exception {}

    public abstract void start();

    public abstract void stop();
    
    public void shutdown() {
        stopped = true;
    }
    
    public String getParameter(String name) {
        return parameter.getParameters().get(name);
    }
    
    public String getParameter(String name, String defaultValue) {
        return parameter.getParameters().getOrDefault(name, defaultValue);
    }

    public File getConfigDir() {

        String userdir = System.getProperty("user.home");
        File dir = new File(userdir, "." + getName());
        if (!dir.exists()) {
            dir.mkdir();
        }

        return dir;

    }

    private void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    private void loop() {

        ExecutorService executor = Executors.newCachedThreadPool();

        executor.submit(new Runnable() {
            @Override
            public void run() {

                while (!stopped) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LineCounterDevice.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                stop();

            }
        });

        executor.shutdown();
    }
    
    private final static class Parameter {
        
        private final Map<String, String> parameters = new HashMap<>();
        
        private Parameter(String... args) {
            for(String arg : args) {
                String[] pair = arg.split("=");
                parameters.put(pair[0].replaceFirst("--", ""), pair[1]);
            }
        }

        public final Map<String, String> getParameters() {
            return parameters;
        }
        
        
    } 

}
