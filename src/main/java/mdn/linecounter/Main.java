package mdn.linecounter;


import static mdn.linecounter.Application.launch;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class Main extends Application  {

    @Override
    public String getName() {
        return "LineCounter";
    }

    @Override
    public void start() {
        LineCounterDevice device = new LineCounterDevice();
        device.boot();
    }

    @Override
    public void stop() {
        
    }
    
    public static void main(String[] args) {        
        launch(args) ;
    }
    
}
