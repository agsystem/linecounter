/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.linecounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.EncodeException;
import mdn.iotlib.comm.message.Message;
import mdn.iotlib.comm.message.MessageListener;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class LineCounterDevice extends IpDeviceBase implements MessageListener {

    private LineCounterButton[] buttons;

    private CommService commService;

    private Queue<Message> queue;

    private volatile boolean registered;

    private final Runnable runnableQueue = new Runnable() {

        @Override
        public void run() {

            while (true) {

                try {

                    Message msg = queue.peek(); // Look at the head of queue.

                    if (msg != null) { // If found a message waiting to be sent.
                        System.out.println("SENDING MESSAGE...");
                        tryToSend(msg); // Blocked until successfully sent the message.
                        System.out.println("Message sent.");

                        queue.poll(); // Remove the message that has been sent.

                    } else { // If queue is empty.
                        System.out.println("Entering wait state...");
                        synchronized (this) {
                            wait();
                        }
                        System.out.println("Leaving wait state.");
                    }

                } catch (InterruptedException ex) {
                    break;
                } catch (IOException | EncodeException ex) {
                    Logger.getLogger(CommService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        public void tryToSend(Message msg) throws IOException, EncodeException {
            
            send(msg);

        }

    };

    public LineCounterDevice() {
        setName(uuid());
    }

    public void boot() {

        setup();

        initCommService();

    }

    private void setup() {

        buttons = new LineCounterButton[]{
            LineCounterButton.prepareWithHost(this).withNumber(1).withInput(Input.IN_01).build(),
            LineCounterButton.prepareWithHost(this).withNumber(2).withInput(Input.IN_02).build(),
            LineCounterButton.prepareWithHost(this).withNumber(3).withInput(Input.IN_03).build(),
            LineCounterButton.prepareWithHost(this).withNumber(4).withInput(Input.IN_04).build(),
            LineCounterButton.prepareWithHost(this).withNumber(5).withInput(Input.IN_05).build(),
            LineCounterButton.prepareWithHost(this).withNumber(6).withInput(Input.IN_06).build(),
            LineCounterButton.prepareWithHost(this).withNumber(7).withInput(Input.IN_07).build(),
            LineCounterButton.prepareWithHost(this).withNumber(8).withInput(Input.IN_08).build(),
            LineCounterButton.prepareWithHost(this).withNumber(9).withInput(Input.IN_09).build(),
            LineCounterButton.prepareWithHost(this).withNumber(10).withInput(Input.IN_10).build(),
            LineCounterButton.prepareWithHost(this).withNumber(11).withInput(Input.IN_11).build(),
            LineCounterButton.prepareWithHost(this).withNumber(12).withInput(Input.IN_12).build()
        };

        queue = new ConcurrentLinkedQueue();
    }

    private void initCommService() {

        try {
            String serverAddress = Application.getInstance().getParameter("server-address", "localhost");
            String serverPort = Application.getInstance().getParameter("server-port", "8080");
            URI serverURI = new URI("ws://" + serverAddress + ":" + serverPort + "/tiger/websocket/linecounter/" + getName());
            commService = new CommService(this, serverURI);
            commService.connect();
        } catch (URISyntaxException ex) {
            Logger.getLogger(LineCounterDevice.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void register() {
        String payload = getIpAddr() + "|" + getHostName();
        Message registerMessage = new Message(payload.getBytes().length);
        registerMessage.setSource(UUID.fromString(getName()));
        registerMessage.setFlag(0);
        registerMessage.putString(payload);

        try {
            send(registerMessage);
            registered = true;
        } catch (IOException ex) {
            Logger.getLogger(LineCounterDevice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EncodeException ex) {
            Logger.getLogger(LineCounterDevice.class.getName()).log(Level.SEVERE, null, ex);
        }

//        buttons[0].test(500);
//        buttons[1].test(500);
//        buttons[2].test(500);
//        buttons[3].test(500);
//        buttons[4].test(500);
//        buttons[5].test(500);
//        buttons[6].test(500);
//        buttons[7].test(500);
//        buttons[8].test(500);
//        buttons[9].test(500);
//        buttons[10].test(500);
//        buttons[11].test(500);
//        buttons[0].test(500);
//        buttons[1].test(500);
//        buttons[2].test(500);
//        buttons[3].test(500);
//        buttons[4].test(500);
//        buttons[5].test(500);
//        buttons[6].test(500);
//        buttons[7].test(500);
//        buttons[8].test(500);
//        buttons[9].test(500);
//        buttons[10].test(500);
//        buttons[11].test(500);
    }

    public void send(Message message) throws IOException, EncodeException {
        commService.send(message);
    }

    @Override
    public void onMessage(Message msg) {

    }

    public void shutdown() {

    }

    public boolean isRegistered() {
        return registered;
    }

    public static String uuid() {

        File configDir = Application.getInstance().getConfigDir();
        File uuidFile = new File(configDir, "uuid");
        String uuid = "";

        if (uuidFile.exists()) {

            try (FileReader fread = new FileReader(uuidFile)) {
                int count;
                char[] buff = new char[16];
                do {
                    count = fread.read(buff);
                    for (int i = 0; i < count; i++) {
                        uuid += buff[i];
                    }
                } while (count != -1);

                fread.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LineCounterDevice.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(LineCounterDevice.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            uuid = UUID.randomUUID().toString().trim();

            try (FileWriter fwriter = new FileWriter(uuidFile)) {
                fwriter.write(uuid, 0, uuid.length());

                fwriter.close();
            } catch (IOException ex) {
                Logger.getLogger(LineCounterDevice.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return uuid;

    }

    public LineCounterButton[] getButtons() {
        return buttons;
    }

    public Queue<Message> getQueue() {
        return queue;
    }

    public Runnable getRunnableQueue() {
        return runnableQueue;
    }

    public void queued(Message msg) {
        getQueue().offer(msg);
        synchronized(runnableQueue) {
            runnableQueue.notify();
        }
    }

}
