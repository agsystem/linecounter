/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.linecounter;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

/**
 *
 * @author Arief Prihasanto
 */
public enum Input {
    IN_01(RaspiPin.GPIO_07, true),
    IN_02(RaspiPin.GPIO_09, false),
    IN_03(RaspiPin.GPIO_08, false),
    IN_04(RaspiPin.GPIO_03, true),
    IN_05(RaspiPin.GPIO_02, true),
    IN_06(RaspiPin.GPIO_00, true),
    IN_07(RaspiPin.GPIO_21, true),
    IN_08(RaspiPin.GPIO_22, true),
    IN_09(RaspiPin.GPIO_23, true),
    IN_10(RaspiPin.GPIO_12, true),
    IN_11(RaspiPin.GPIO_13, true),
    IN_12(RaspiPin.GPIO_14, true);
    
    private Pin pin = null;
    private boolean inverted = true;
    
    private Input(Pin pin, boolean inverted) {
        this.pin = pin;
        this.inverted = inverted;
    }

    public Pin getPin() {
        return pin;
    }

    public boolean isInverted() {
        return inverted;
    }
}
