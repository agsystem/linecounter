# README #

This is IoT project utilizing Raspberry Pi as a data collector equipment. It send tick data of the production line to the main app (https://bitbucket.org/agsystem/tiger) so the data can be displayed to the displayer (https://bitbucket.org/agsystem/versatiledisplayer) .

### How do I get set up? ###
1. Make this Raspi add-on board (https://bitbucket.org/agsystem/linecounter-pcb).
2. Install add-on board into Raspi2 B+ board.
1. Install Java 1.8 to your Raspi
2. Compile the app.
3. Make sure the main app and displayer was running and already setup the line counter feature.
4. Transfer it into your Raspi.
3. Connect your board to DHCP based network.
4. RUn: java -jar LineCounter.jar --server-address=ip_address_of_the_computer_running_main_app --server-port=port_used_by_the_computer_running_main_app.
5. Done.
